<?php

/**
 * Customised WordPress Settings API
 *
 * @author
 */
if ( !class_exists('WPTFC_Settings_API' ) ):
class WPTFC_Settings_API {

    private $settings_api;

    function __construct() {

        $this->settings_api = new WeDevs_Settings_API;

        add_action( 'admin_init', array( $this, 'wptfc_initialise_settings_page') );
        add_action( 'admin_menu', array( $this, 'wptfc_admin_menu') );

    }

    function wptfc_initialise_settings_page() {

        //set the settings
        $this->settings_api->set_sections( $this->get_settings_sections() );
        $this->settings_api->set_fields( $this->get_settings_fields() );
        //initialize settings
        $this->settings_api->admin_init();

    }

    function wptfc_admin_menu() {
        add_options_page( 'WP Twitter Feeds', 'WP Twitter Feeds', 'delete_posts', 'wptfc_settings_page', array($this, 'wptfc_settings_page_callback') );
    }

    function get_settings_sections() {
        $sections = array(
            array(
                'id'    => 'wptfc_connect_twitter',
                'title' => __( 'Connect Twitter', 'wptfc' )
            ),
            array(
                'id'    => 'wptfc_general',
                'title' => __( 'General', 'wptfc' )
            ),
            array(
                'id'    => 'wptfc_styling',
                'title' => __( 'Styling', 'wptfc' )
            ),
            array(
                'id'    => 'wptfc_caching',
                'title' => __( 'Caching', 'wptfc' )
            ),
            array(
                'id'    => 'wptfc_developer',
                'title' => __( 'Developer', 'wptfc' )
            ),
        );
        return $sections;
    }
    /**
     * Returns all the settings fields
     *
     * @return array settings fields
     */
    function get_settings_fields() {
        $settings_fields = array(
            'wptfc_connect_twitter' => array(
                array(
                    'name'              => 'wptfc_consumer_key',
                    'label'             => __( 'Consumer Key', 'wptfc' ),
                    'desc'              => __( 'Text input description', 'wptfc' ),
                    'placeholder'       => __( 'xxxxxxxxxxxxxxxxxx', 'wptfc' ),
                    'type'              => 'text',
                    'default'           => '',
                    'sanitize_callback' => 'sanitize_text_field'
                ),
                array(
                    'name'              => 'wptfc_consumer_secret',
                    'label'             => __( 'Consumer Secret', 'wptfc' ),
                    'desc'              => __( 'Text input description', 'wptfc' ),
                    'placeholder'       => __( 'xxxxxxxxxxxxxxxxxx', 'wptfc' ),
                    'type'              => 'text',
                    'default'           => '',
                    'sanitize_callback' => 'sanitize_text_field'
                ),
                array(
                    'name'              => 'wptfc_access_token',
                    'label'             => __( 'Access Token', 'wptfc' ),
                    'desc'              => __( 'Text input description', 'wptfc' ),
                    'placeholder'       => __( 'xxxxxxxxxxxxxxxxxx', 'wptfc' ),
                    'type'              => 'text',
                    'default'           => '',
                    'sanitize_callback' => 'sanitize_text_field'
                ),
                array(
                    'name'              => 'wptfc_access_token_secret',
                    'label'             => __( 'Access Token Secret', 'wptfc' ),
                    'desc'              => __( 'Text input description', 'wptfc' ),
                    'placeholder'       => __( 'xxxxxxxxxxxxxxxxxx', 'wptfc' ),
                    'type'              => 'text',
                    'default'           => '',
                    'sanitize_callback' => 'sanitize_text_field'
                ),
                array(
                    'name'              => 'wptfc_twitter_username',
                    'label'             => __( 'Twitter Username', 'wptfc' ),
                    'desc'              => __( 'Text input description', 'wptfc' ),
                    'placeholder'       => __( '@username', 'wptfc' ),
                    'type'              => 'text',
                    'default'           => '',
                    'sanitize_callback' => 'sanitize_text_field'
                ),

            ),
            'wptfc_general' => array(
                array(
                    'name'              => 'wptfc_section_title',
                    'label'             => __( 'Title', 'wptfc' ),
                    'desc'              => __( 'Text input description', 'wptfc' ),
                    'placeholder'       => __( 'Latest Tweets', 'wptfc' ),
                    'type'              => 'text',
                    'default'           => 'Latest Tweets',
                    'sanitize_callback' => 'sanitize_text_field'
                ),
                array(
                    'name'  => 'wptfc_display_tweet_replies',
                    'label' => __( 'Display Tweet Replies', 'wptfc' ),
                    'desc'  => __( 'Enable', 'wptfc' ),
                    'type'  => 'checkbox'
                ),
                array(
                    'name'  => 'wptfc_display_tweet_retweets',
                    'label' => __( 'Display Tweet Retweets', 'wptfc' ),
                    'desc'  => __( 'Enable', 'wptfc' ),
                    'type'  => 'checkbox'
                ),
            ),
            'wptfc_styling' => array(
                array(
                    'name'    => 'wptfc_wrapper_bgcolor',
                    'label'   => __( 'Background Color', 'wptfc' ),
                    'desc'    => __( 'Color description', 'wptfc' ),
                    'type'    => 'color',
                    'default' => ''
                ),

            ),
            'wptfc_caching' => array(
                array(
                    'name'  => 'wptfc_enable_cache',
                    'label' => __( 'Tweets Caching', 'wptfc' ),
                    'desc'  => __( 'Enable', 'wptfc' ),
                    'type'  => 'checkbox'
                ),
                array(
                    'name'    => 'wptfc_cache_time_interval',
                    'label'   => __( 'Cache Time Interval', 'wedevs' ),
                    'desc'    => __( 'Dropdown description', 'wedevs' ),
                    'type'    => 'select',
                    'default' => 'no',
                    'options' => array(
                        '1' => '1 Hour',
                        '2'  => '2 Hours',
                        '3' => '3 Hours',
                        '4'  => '4 Hours',
                        '5'  => '5 Hours',
                    )
                ),
            ),
            'wptfc_developer' => array(
                array(
                    'name'  => 'wptfc_enable_debug',
                    'label' => __( 'Start Debugging', 'wptfc' ),
                    'desc'  => __( 'Enable', 'wptfc' ),
                    'type'  => 'checkbox'
                ),
            ),
        );
        return $settings_fields;
    }
    function wptfc_settings_page_callback() {
        echo '<div class="wrap">';
        $this->settings_api->show_navigation();
        $this->settings_api->show_forms();
        echo '</div>';
    }
    /**
     * Get all the pages
     *
     * @return array page names with key value pairs
     */
    function get_pages() {
        $pages = get_pages();
        $pages_options = array();
        if ( $pages ) {
            foreach ($pages as $page) {
                $pages_options[$page->ID] = $page->post_title;
            }
        }
        return $pages_options;
    }
}
endif;

/*
array(
                    'name'              => 'number_input',
                    'label'             => __( 'Number Input', 'wedevs' ),
                    'desc'              => __( 'Number field with validation callback `floatval`', 'wedevs' ),
                    'placeholder'       => __( '1.99', 'wedevs' ),
                    'min'               => 0,
                    'max'               => 100,
                    'step'              => '0.01',
                    'type'              => 'number',
                    'default'           => 'Title',
                    'sanitize_callback' => 'floatval'
                ),
                array(
                    'name'        => 'textarea',
                    'label'       => __( 'Textarea Input', 'wedevs' ),
                    'desc'        => __( 'Textarea description', 'wedevs' ),
                    'placeholder' => __( 'Textarea placeholder', 'wedevs' ),
                    'type'        => 'textarea'
                ),
                array(
                    'name'        => 'html',
                    'desc'        => __( 'HTML area description. You can use any <strong>bold</strong> or other HTML elements.', 'wedevs' ),
                    'type'        => 'html'
                ),
                array(
                    'name'  => 'checkbox',
                    'label' => __( 'Checkbox', 'wedevs' ),
                    'desc'  => __( 'Checkbox Label', 'wedevs' ),
                    'type'  => 'checkbox'
                ),
                array(
                    'name'    => 'radio',
                    'label'   => __( 'Radio Button', 'wedevs' ),
                    'desc'    => __( 'A radio button', 'wedevs' ),
                    'type'    => 'radio',
                    'options' => array(
                        'yes' => 'Yes',
                        'no'  => 'No'
                    )
                ),
                array(
                    'name'    => 'selectbox',
                    'label'   => __( 'A Dropdown', 'wedevs' ),
                    'desc'    => __( 'Dropdown description', 'wedevs' ),
                    'type'    => 'select',
                    'default' => 'no',
                    'options' => array(
                        'yes' => 'Yes',
                        'no'  => 'No'
                    )
                ),
                array(
                    'name'    => 'password',
                    'label'   => __( 'Password', 'wedevs' ),
                    'desc'    => __( 'Password description', 'wedevs' ),
                    'type'    => 'password',
                    'default' => ''
                ),
                array(
                    'name'    => 'file',
                    'label'   => __( 'File', 'wedevs' ),
                    'desc'    => __( 'File description', 'wedevs' ),
                    'type'    => 'file',
                    'default' => '',
                    'options' => array(
                        'button_label' => 'Choose Image'
                    )
                )


                array(
                    'name'    => 'password',
                    'label'   => __( 'Password', 'wedevs' ),
                    'desc'    => __( 'Password description', 'wedevs' ),
                    'type'    => 'password',
                    'default' => ''
                ),
                array(
                    'name'    => 'wysiwyg',
                    'label'   => __( 'Advanced Editor', 'wedevs' ),
                    'desc'    => __( 'WP_Editor description', 'wedevs' ),
                    'type'    => 'wysiwyg',
                    'default' => ''
                ),
                array(
                    'name'    => 'multicheck',
                    'label'   => __( 'Multile checkbox', 'wedevs' ),
                    'desc'    => __( 'Multi checkbox description', 'wedevs' ),
                    'type'    => 'multicheck',
                    'default' => array('one' => 'one', 'four' => 'four'),
                    'options' => array(
                        'one'   => 'One',
                        'two'   => 'Two',
                        'three' => 'Three',
                        'four'  => 'Four'
                    )
                ),
                */
