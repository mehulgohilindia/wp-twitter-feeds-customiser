<?php
/*
 *  Plugin Name: WP Twitter Feeds Customiser
 *  Version: 1.0
 */


# Defining Constants
define('WPTFC_VERSION','1.0');
define("WPTFC_PATH", realpath(dirname(__FILE__) ));
define("WPTFC_BASENAME", plugin_basename(__FILE__));

require_once(WPTFC_PATH.'/admin/class.settings-api.php');
require_once(WPTFC_PATH.'/admin/admin-settings.php');
require_once(WPTFC_PATH.'/frameworks/tweet-php/TweetPHP.php');
require_once(WPTFC_PATH.'/inc/shortcodes.php');
new WPTFC_Settings_API();
