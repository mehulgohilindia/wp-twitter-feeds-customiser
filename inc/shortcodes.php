<?php
/*
 *  Shortcode [wptfc_display_feeds]
 *  This shortcode is used to fetch twitter feeds
 *
 *  @since 1.0
 */

add_shortcode( 'wptfc_display_feeds', 'wptfc_display_feeds_callback');

function wptfc_display_feeds_callback($atts){

    $atts = shortcode_atts(
		array(
			'no_of_tweets' => '10',
			'retrieve_no_of_tweets' => '25',
		), $atts, 'wptfc_display_feeds' );

    $connection_settings = get_option('wptfc_connect_twitter');
    $general_settings = get_option('wptfc_general');
    $styling_settings = get_option('wptfc_styling');
    $caching_settings = get_option('wptfc_caching');
    $developer_settings = get_option('wptfc_developer');

    //echo "<pre>"; print_r($connection_settings);echo "</pre>";

    $caching = 'false';
    if(isset($caching_settings['wptfc_enable_cache']) && $caching_settings['wptfc_enable_cache'] == 'on'){
        $caching = 'true';
    }

    $cache_time_interval = $caching_settings['wptfc_cache_time_interval'];

    $debug = 'false';
    if(isset($developer_settings['wptfc_enable_debug']) && $developer_settings['wptfc_enable_debug'] == 'on'){
        $debug = 'true';
    }

    $twitter_replies = 'true';
    if(isset($general_settings['wptfc_display_tweet_replies']) && $general_settings['wptfc_display_tweet_replies'] == 'on'){
        $twitter_replies = 'false';
    }

    $twitter_retweets = 'true';
    if(isset($general_settings['wptfc_display_tweet_retweets']) && $general_settings['wptfc_display_tweet_retweets'] == 'on'){
        $twitter_retweets = 'false';
    }

    $TweetPHP = new TweetPHP(array(
        'consumer_key'              => $connection_settings['wptfc_consumer_key'],
        'consumer_secret'           => $connection_settings['wptfc_consumer_secret'],
        'access_token'              => $connection_settings['wptfc_access_token'],
        'access_token_secret'       => $connection_settings['wptfc_access_token_secret'],
        'twitter_screen_name'       => $connection_settings['wptfc_twitter_username'],
        'enable_cache'              => $caching,
        'cache_dir'                 => dirname(__FILE__) . '/cache/',
        'cachetime'                 => 60 * 60 * $cache_time_interval,
        'tweets_to_retrieve'        => $atts['retrieve_no_of_tweets'], // Specifies the number of tweets to try and fetch, up to a maximum of 200
        'tweets_to_display'         => $atts['no_of_tweets'], // Number of tweets to display
        'ignore_replies'            => $twitter_replies,
        'ignore_retweets'           => $twitter_retweets,
        'twitter_style_dates'       => false, // Use twitter style dates e.g. 2 hours ago
        'twitter_date_text'         => array('seconds', 'minutes', 'about', 'hour', 'ago'),
        'date_format'               => '%I:%M %p %b %e%O', // The defult date format e.g. 12:08 PM Jun 12th. See: http://php.net/manual/en/function.strftime.php
        'date_lang'                 => 'en_EN', // Language for date e.g. 'fr_FR'. See: http://php.net/manual/en/function.setlocale.php
        'twitter_template'          => '<h2>' . $general_settings['wptfc_section_title'] . '</h2><ul id="twitter">{tweets}</ul>',
        'tweet_template'            => '<li><span class="status">{tweet}</span><span class="meta"><a href="{link}">{date}</a></span></li>',
        'error_template'            => '<li><span class="status">Our twitter feed is unavailable right now.</span> <span class="meta"><a href="{link}">Follow us on Twitter</a></span></li>',
        'debug'                 => $debug
    ));

    $tweets =  $TweetPHP->get_tweet_list();
    echo $tweets;

}
